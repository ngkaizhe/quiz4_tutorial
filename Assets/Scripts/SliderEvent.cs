﻿using UnityEngine;
using System.Collections;

public class SliderEvent : MonoBehaviour {

    public void SliderChange(float value)
    {
        print("Slider Changed!! pass value : " + value);
    }

    public void SliderChange(int value)
    {
        print("Slider Changed!! pass value : " + value);
    }

    public void SliderChange(GameObject go)
    {
        print("Slider Changed!! GameObject name : " + go.name);
    }

    public void SliderChange(Camera cam)
    {
        print("Slider Changed!! Camera name : " + cam.name);
    }
}
