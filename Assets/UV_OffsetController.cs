﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UV_OffsetController : MonoBehaviour
{
    public Material m_Material;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float uv_x = m_Material.GetVector("_Offset").x;
        float uv_y = m_Material.GetVector("_Offset").y;

        uv_x += Time.deltaTime * 0.1f;
        uv_y += Time.deltaTime * 0.1f;

        m_Material.SetVector("_Offset", new Vector4(uv_x, uv_y, 0.0f, 0.0f));
    }
}
