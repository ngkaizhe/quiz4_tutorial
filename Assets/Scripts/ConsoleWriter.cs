﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConsoleWriter : MonoBehaviour {

	public void PrintString(string msg) {
		Debug.Log (msg);
	}

	public void PrintBoolean(bool b) {
		Debug.Log (b);
	}

}
