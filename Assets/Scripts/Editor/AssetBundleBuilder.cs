﻿using UnityEditor;
using UnityEngine;
using System.IO;


public class AssetBundleBuilder
{
    // New AssetBundle builder in menu item
    [MenuItem("Assets/AssetBundle Builders/Build All AssetBundles (Windows x64)")]
    static void CreateAssetBundles_win_x64()
    {
        string assetBundleDir = "Assets/StreamingAssets";

        if (!Directory.Exists(Application.streamingAssetsPath))
        {
            Directory.CreateDirectory(assetBundleDir);
        }

        // Build AssetBundle and save in local directory for windows x64
        BuildPipeline.BuildAssetBundles(assetBundleDir,
            BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
    }
}
