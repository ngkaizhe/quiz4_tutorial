﻿using UnityEngine;
using System.Collections;

public class ButtonEvent : MonoBehaviour {
    
    public void ButtonClick()
    {
        print("Button Clicked!!");
    }

    public void ButtonClick(int value)
    {
        print("Button Clicked!! pass value : " + value);
    }

    public void ButtonClick(GameObject go)
    {
        print("Button Clicked!! GameObject name : " + go.name);
    }

    public void ButtonClick(Camera cam)
    {
        print("Button Clicked!! Camera name : " + cam.name);
    }
}
