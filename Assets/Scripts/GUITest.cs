﻿using UnityEngine;
using System.Collections;

public class GUITest : MonoBehaviour
{

	void OnGUI()
    {
        GUI.Box(new Rect(10, 10, 100, 90), "TEST");

        if(GUI.Button(new Rect(20, 40, 80, 50), "Button 1"))
        {
            print("GUI Button 1 Pressed");
        }
    }
}
