﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class BundleObjectLoader : MonoBehaviour
{
    public string assetName = "Canvas";
    public string bundleName = "testbundle1";

    // Start is called before the first frame update
    async void Start()
    {
        // load to get bundle first
        AssetBundleCreateRequest asyncBundleRequest = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, bundleName));
        await asyncBundleRequest;

        AssetBundle localAssetBundle = asyncBundleRequest.assetBundle;

        if (localAssetBundle == null)
        {
            Debug.LogError("Failed to load AssetBundle!");
            return;
        }

        AssetBundleRequest assetRequest = localAssetBundle.LoadAssetAsync<GameObject>(assetName);
        await assetRequest;

        // load asset from specific bundle
        GameObject prefab = assetRequest.asset as GameObject;
        Instantiate(prefab);
        localAssetBundle.Unload(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
