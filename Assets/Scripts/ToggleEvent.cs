﻿using UnityEngine;
using System.Collections;

public class ToggleEvent : MonoBehaviour {

    public void ToggleCheck(bool value)
    {
        print("Toggle Status : " + value);
    }

    public void ToggleChange(int value)
    {
        print("Toggle Changed!! pass value : " + value);
    }

    public void ToggleChange(GameObject go)
    {
        print("Toggle Changed!! GameObject name : " + go.name);
    }

    public void ToggleChange(Camera cam)
    {
        print("Toggle Changed!! Camera name : " + cam.name);
    }
}
